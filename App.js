/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import type { Node,   } from 'react';
import {Platform, StyleSheet, View} from 'react-native'

import {WebViewScreen} from './screens/WebView';
import {SplashScreen} from './screens/Splash';

const App: () => Node = () => {

  const url = 'http://www.terbah.sa'

  onMessage = (message) => {
    console.log('action result coming from the webview: ', message);
  };

   const getContent = (props)=>{
    if(showSplash) {
      return (<SplashScreen/>)
    }else{
      return (<WebViewScreen/>)
    }
  }


 
  return (
    <View style={styles.App_RootView}>

      <WebViewScreen />

     {/* <WebView
        source={{ uri: url }}
        style={styles.WebViewScreen_RootView}
        originWhitelist={['https://*', 'http://*']}
        bounces={false}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        javaScriptEnabled={true}
        originWhitelist={['*']}
        onMessage={m => this.onMessage(m)}
      /> */}

      </View>
  );
};

const styles = StyleSheet.create(  
{  
    App_RootView: {
      flex: 1,  
        paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0
    },
    WebViewScreen_RootView:  
    {  
        flex:1,  
        position: 'absolute',  
        width: '100%',  
        height: '100%'  
      }, 
  
});  

export default App;
