import React, { Component } from 'react';
import {
  Alert,
  BackHandler,
  Platform,
  Linking
} from 'react-native';


import { WebView } from 'react-native-webview';

import RNBootSplash from "react-native-bootsplash";

export class WebViewScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { timerComplete: false, webLoaded: false };
  }

  webView = {
    canGoBack: false,
    ref: null,
  }

  url = 'https://terbah.sa/'


  componentDidMount() {
    const timeout = setTimeout(() => {
      this.onTimerReached()
      clearTimeout(timeout)
    }, 2000)
  }

  onAndroidBackPress = () => {
    if (this.webView.canGoBack && this.webView.ref) {
      this.webView.ref.goBack();
      return true;
    }
    Alert.alert(
      'Exit App',
      'Exiting the application?', [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      }, {
        text: 'OK',
        onPress: () => BackHandler.exitApp()
      },], {
      cancelable: false
    }
    )
    return true;
  }



  componentWillMount() {
    if (true || Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.onAndroidBackPress);
    }
  }

  componentWillUnmount() {
    if (true || Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress');
    }
  }

  onWebLoadError = (error) => {
    const { nativeEvent } = error;
    console.error(nativeEvent)

    if (nativeEvent.url.startsWith('tel:') && nativeEvent.canGoBack) {
      // Fallback for tel links without target="_blank"
      this.webView.ref.goBack();
    } else {
      Alert.alert(
        'Error Loading App',
        'Something went wrong! Check your internet connection and try again!', [{
          text: 'Exit',
          onPress: () => BackHandler.exitApp(),
          style: 'cancel'
        }, {
          text: 'Try Again!',
          onPress: () => this.webView.ref.reload()
        },], {
        cancelable: false
      }
      )
    }


  }

  onTimerReached = () => {
    console.log('Timer reached')
    this.setState({ timerComplete: true });
  }

  onWebViewLoaded = () => {
    console.log('Web Content Loaded')
    this.setState({ webLoaded: true });
  }

  onShouldStartLoadWithRequest(request) {

    // short circuit these
    if (!request.url ||
      request.url.startsWith('http') ||
      request.url.startsWith("/") ||
      request.url.startsWith("#") ||
      request.url.startsWith("javascript") ||
      request.url.startsWith("about:blank")
    ) {
      return true;
    }

    // blocked blobs
    if (request.url.startsWith("blob")) {
      Alert.alert("Link cannot be opened.");
      return false;
    }

    // list of schemas we will allow the webview
    // to open natively
    if (request.url.startsWith("tel:") ||
      request.url.startsWith("mailto:") ||
      request.url.startsWith("maps:") ||
      request.url.startsWith("geo:") ||
      request.url.startsWith("sms:")
    ) {

      Linking.openURL(request.url).catch(er => {
        Alert.alert("Failed to open Link: " + er.message);
      });
      return false;
    }

    // let everything else to the webview
    return true;
  }



  render() {
    if (this.state.webLoaded && this.state.timerComplete) {
      RNBootSplash.hide({ fade: true });
    }
    return (
      <WebView
        source={{ uri: this.url }}
        onLoad={(s) => { this.onWebViewLoaded() }}
        onShouldStartLoadWithRequest={(r) => { return this.onShouldStartLoadWithRequest(r) }}
        ref={(webView) => { this.webView.ref = webView; }}
        onError={(error) => { this.onWebLoadError(error) }}
        showsHorizontalScrollIndicator={false}
        onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack; }}
        showsVerticalScrollIndicator={false}
        originWhitelist={['*']}
      />
    );
  }
}


export default WebViewScreen;